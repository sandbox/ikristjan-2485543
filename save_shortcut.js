/**
 * @file
 * Attaches behaviors for the Save Shortcut module.
 */

(function ($) {

/**
 * Bind the save shortcut.
 */  
Drupal.behaviors.save_shortcut = {
  attach: function (context) {
    // Make sure it's triggered only once.
    $(document).unbind('keydown.saveShortcut').bind('keydown.saveShortcut', function(e) {
      // Bind only ctrl+s and cmd+s (for Mac) shortcuts.
      if (e.keyCode == 83 && (navigator.platform.match('Mac') ? e.metaKey : e.ctrlKey)) {
        e.preventDefault();
        
        // Find first real actionable submit button which is not ajax based, and trigger that form.
        $('.form-actions .form-submit:visible').not('.ajax-processed').first().closest('form').trigger('submit');
      }
    });
  }
};

})(jQuery);
